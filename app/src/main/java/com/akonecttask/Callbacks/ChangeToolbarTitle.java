package com.akonecttask.Callbacks;

/**
 * Interface to change the toolbar title and
 * show its visibility. Will be implemented in the
 * MainActivity.java file.
 */

public interface ChangeToolbarTitle {

    void onChangeToolbarTitle(String title, boolean visibility);
}
