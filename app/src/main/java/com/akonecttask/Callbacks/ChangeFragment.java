package com.akonecttask.Callbacks;

/**
 * Created by ${Rajeev} on 7/1/2017.
 */

public interface ChangeFragment {

    public void callFragment(String fragmentName);

}
