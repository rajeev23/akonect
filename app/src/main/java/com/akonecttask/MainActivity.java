package com.akonecttask;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.akonecttask.Callbacks.ChangeFragment;
import com.akonecttask.Callbacks.ChangeToolbarTitle;
import com.akonecttask.Fragments.AddBillFragment;
import com.akonecttask.Fragments.BillSummaryFragment;
import com.akonecttask.Utility.LoggedInUserStore;
import com.akonecttask.Utility.Utility;

public class MainActivity extends AppCompatActivity implements ChangeToolbarTitle, ChangeFragment {
    private final static String TAG = MainActivity.class.getSimpleName();
    Toolbar toolbar;
    TextView toolbar_title;
    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       /* getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
*/
        LoggedInUserStore.setUserId(this, 1);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        /*setting up the toolbar and the textview widgets*/
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) findViewById(R.id.tb_title);

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(Utility.getDrawable(this, R.drawable.ic_backpress_arrow));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int count = getSupportFragmentManager().getBackStackEntryCount();
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStack();
                } else {
                    finish();
                }
            }
        });


        BillSummaryFragment billSummaryFragment = new BillSummaryFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putString("Message", "success");
        billSummaryFragment.setArguments(bundle);
        ft.add(R.id.fragmentController, billSummaryFragment, "BILL_SUMMARY_FRAGMENT");
        ft.commit();

        // openFragment(getIntent().getStringExtra("fragmentName"));

    }

    @Override
    public void onChangeToolbarTitle(String title, boolean visibility) {
        toolbar_title.setText(title);
        if (visibility) {
            toolbar.setVisibility(View.VISIBLE);
        } else {
            toolbar.setVisibility(View.GONE);
        }
    }

    @Override
    public void callFragment(String fragmentName) {
        Bundle bundle = null;
        switch (fragmentName) {
            case "ADD_BILL_FRAGMENT":
                fragment = new AddBillFragment();
                bundle = new Bundle();
                bundle.putString("message", "success");
                fragment.setArguments(bundle);


                break;
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentController, fragment)
                .addToBackStack(null).commit();

    }
}
