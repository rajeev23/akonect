package com.akonecttask.Fragments;


import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.akonecttask.Callbacks.ChangeToolbarTitle;
import com.akonecttask.Database.DatabaseHelper;
import com.akonecttask.R;
import com.akonecttask.Utility.LoggedInUserStore;
import com.akonecttask.Utility.Utility;
import com.akonecttask.adapters.NothingSelectedSpinnerAdapter;
import com.akonecttask.pojo.UserPojo;

import java.util.ArrayList;

/**
 * The add bill fragment will allow the user to
 * add a bill. It will contain a TextField for the AMOUNT
 * A spinner from which the user will have to select a
 * person with whom the bill was split with.
 * and a checkbox to specify if the bill was paid by the user adding the bill
 * or by the user in the dropdown.
 */
public class AddBillFragment extends Fragment implements View.OnClickListener {
    private final static String TAG = AddBillFragment.class.getSimpleName();

    ChangeToolbarTitle changeToolbarTitle;
    Typeface tf;

    EditText amountTxt;
    Spinner personsSpinner;
    CheckBox isPaidByCb;
    Button saveBillBtn;

    UserPojo userPojo;
    ArrayList<UserPojo> userPojoArrayList;
    ArrayList<String> userNameList;
    DatabaseHelper myDb;

    public AddBillFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        changeToolbarTitle = (ChangeToolbarTitle) context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_bill, container, false);

        changeToolbarTitle.onChangeToolbarTitle("Add Bill", true);
        tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Helvetica1.otf");
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        myDb = new DatabaseHelper(getActivity());
        Cursor cursor = myDb.getAllusers();

        if (cursor.getCount() == 0) {
            //show error message
            Toast.makeText(getContext(), "No Data found!", Toast.LENGTH_SHORT).show();
        } else {

            userPojoArrayList = new ArrayList<>();
            while (cursor.moveToNext()) {

                userPojo = new UserPojo();
                userPojo.setUserId(cursor.getInt(0));
                userPojo.setUsername(cursor.getString(1));
                userPojoArrayList.add(userPojo);
            }
        }


        setUpWidgets(view);


        return view;
    }

    private void setUpWidgets(View view) {
        amountTxt = (EditText) view.findViewById(R.id.amountText);
        personsSpinner = (Spinner) view.findViewById(R.id.personSpinner);
        isPaidByCb = (CheckBox) view.findViewById(R.id.paidCb);
        saveBillBtn = (Button) view.findViewById(R.id.saveBtn);

        amountTxt.setTypeface(tf);
        isPaidByCb.setTypeface(tf);
        saveBillBtn.setTypeface(tf);

        saveBillBtn.setOnClickListener(this);
        userNameList = new ArrayList<>();

        if (userPojoArrayList != null) {
            for (int i = 1; i < userPojoArrayList.size(); i++) {
                userNameList.add(userPojoArrayList.get(i).getUsername());
            }
        }

        final ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_layout, userNameList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        personsSpinner.setAdapter(new NothingSelectedSpinnerAdapter(adapter, R.layout.custom_spinner_dialog_layout, getActivity()));


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.saveBtn:
            /*Save data to local db*/

                if (validate()) {

                    String amount = amountTxt.getText().toString();
                    float amountFloat = Float.parseFloat(amount);

                    //Person who the bill is shared with. His user_id
                    int userId = userPojoArrayList.get(personsSpinner.getSelectedItemPosition()).getUserId();
                   /*get the bill id in return */
                    long bill_id = myDb.saveBill(amountFloat, LoggedInUserStore.getUserId(getContext()));
                    boolean paidBy = false;
                    if (isPaidByCb.isChecked()) {
                        paidBy = true;
                    }

                    if (bill_id > 0) {
                        myDb.saveSplit(bill_id, userId, LoggedInUserStore.getUserId(getContext()), paidBy);
                        getFragmentManager().popBackStack();
                    } else {
                        Toast.makeText(getContext(), "Error Occured!", Toast.LENGTH_SHORT).show();
                    }


                }

                break;
        }
    }

    private boolean validate() {

        if (amountTxt.getText().toString().trim().length() <= 0) {
            amountTxt.setError("Cant be empty!");
            return false;
        } else if (personsSpinner.getSelectedItem() == null) {
            Utility.showToast(getContext(), "Share with cannot be empty!");
            return false;
        } else {
            return true;
        }
    }
}
