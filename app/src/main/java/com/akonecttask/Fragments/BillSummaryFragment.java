package com.akonecttask.Fragments;


import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.akonecttask.Callbacks.ChangeFragment;
import com.akonecttask.Callbacks.ChangeToolbarTitle;
import com.akonecttask.Database.DatabaseHelper;
import com.akonecttask.R;
import com.akonecttask.Utility.LoggedInUserStore;
import com.akonecttask.adapters.BillSummaryAdapter;
import com.akonecttask.pojo.AmountGetPojo;

import java.util.ArrayList;

/**
 * This fragment will show the list of all the bills
 * basically a summary of all the bills.
 */
public class BillSummaryFragment extends Fragment implements View.OnClickListener {

    private final static String TAG = BillSummaryFragment.class.getSimpleName();

    ChangeToolbarTitle changeToolbarTitle;
    Typeface tf;
    TextView oweTextView, getTextView, balanceTextView;
    FloatingActionButton addBillBtn;
    RecyclerView billSummaryRv;
    ChangeFragment changeFragmentCallback;

    float amountOwed, amountGet, totalBalance;

    DatabaseHelper myDb;

    ArrayList<AmountGetPojo> getAmountsList;

    public BillSummaryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        changeToolbarTitle = (ChangeToolbarTitle) context;
        changeFragmentCallback = (ChangeFragment) context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_bill_summary, container, false);
        changeToolbarTitle.onChangeToolbarTitle("Bill Summary", true);
        tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Helvetica1.otf");
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        setUpWidgets(view);
        addBillBtn.setOnClickListener(this);
        myDb = new DatabaseHelper(getActivity());

        // myDb.deletetables();

        return view;

    }


    @Override
    public void onStart() {
        super.onStart();
        amountOwed = 0;
        amountGet = 0;
        totalBalance = 0;
        getAmountsList = new ArrayList<>();
        Cursor cursor = myDb.fetchData(getContext());
        if (cursor.getCount() == 0) {
            Toast.makeText(getContext(), "NO Data", Toast.LENGTH_SHORT).show();
        } else {
            while (cursor.moveToNext()) {
                getBillSummary(cursor);
            }
            totalBalance = amountGet - amountOwed;
            oweTextView.setText(String.valueOf(amountOwed));
            getTextView.setText(String.valueOf(amountGet));
            balanceTextView.setText(String.valueOf(totalBalance));
            populateBillSummaryView();

        }

    }

    /*This method will populate the recycler view for the BillSummary fragment.
    * this will make use of an adapter that will populate the recycler view.
    * We will pass our getAmountList<getAmountPojo>*/
    private void populateBillSummaryView() {

        BillSummaryAdapter billSummaryAdapter = new BillSummaryAdapter(getContext(), getAmountsList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false);
        billSummaryRv.setLayoutManager(layoutManager);
        billSummaryRv.setAdapter(billSummaryAdapter);

    }


    /*This method will get all the bil summary, and will store the data into an arraylist
    * of type AmountGetPojo which will contain fields like userId, amount, userName, status
    * The Status is used to specify if the amount is owed by the user or if the amount is got by the user.
    * */
    private void getBillSummary(Cursor cursor) {

        String[] userIdArr = cursor.getString(0).split(",");
        String[] userNameArr = cursor.getString(1).split(",");
        String[] paidByIdArr = cursor.getString(2).split(",");
        String amount = cursor.getString(6);

        AmountGetPojo amountGetPojo = new AmountGetPojo();

        //Divide the amount by 2 since it is shared by 2 users
        float amountFloat = Float.parseFloat(amount) / 2;

        //if the userIdArr[0] is equal to 1(user ID of the person who created the bill )
        //Then we check if the paidById is 1, If so that means the user who created the bill
        //has paid for that bill and hence will get the money.
        //Else , if the paidById is equal to zero then the user who created the bill will owe
        //the other User the required amount of money.
        if (userIdArr[0].equalsIgnoreCase(String.valueOf(LoggedInUserStore.getUserId(getContext())))) {

            if (paidByIdArr[0].equalsIgnoreCase("1")) {

                //GET Money
                amountGetPojo.setUserId(userIdArr[0]);
                amountGetPojo.setStatus("get");
                amountGetPojo.setUserName(userNameArr[0]);
                amountGetPojo.setAmount(amountFloat);
                amountGetPojo.setOther(userNameArr[1]);
                amountGet += amountFloat;
                getAmountsList.add(amountGetPojo);


            } else {

                //OWE Money
                amountGetPojo.setUserId(userIdArr[1]);
                amountGetPojo.setStatus("owed");
                amountGetPojo.setOther(userNameArr[0]);
                amountGetPojo.setAmount(amountFloat);
                amountGetPojo.setUserName(userNameArr[1]);
                amountOwed += amountFloat;
                getAmountsList.add(amountGetPojo);
            }


        } else {
            if (paidByIdArr[1].equalsIgnoreCase("1")) {
                amountGetPojo.setUserId(userIdArr[1]);
                amountGetPojo.setStatus("get");
                amountGetPojo.setOther(userNameArr[0]);
                amountGetPojo.setAmount(amountFloat);
                amountGetPojo.setUserName(userNameArr[1]);
                //amountGetPojo.setAmount();
                amountGet += amountFloat;
                getAmountsList.add(amountGetPojo);
            } else {
                amountGetPojo.setUserId(userIdArr[0]);
                amountGetPojo.setStatus("get");
                amountGetPojo.setUserName(userNameArr[0]);
                amountGetPojo.setAmount(amountFloat);
                amountGetPojo.setOther(userNameArr[1]);
                amountOwed += amountFloat;
                getAmountsList.add(amountGetPojo);

            }
        }

    }

    private void setUpWidgets(View view) {
        oweTextView = (TextView) view.findViewById(R.id.oweText);
        getTextView = (TextView) view.findViewById(R.id.getText);
        balanceTextView = (TextView) view.findViewById(R.id.balanceText);
        addBillBtn = (FloatingActionButton) view.findViewById(R.id.add_bill_btn);
        billSummaryRv = (RecyclerView) view.findViewById(R.id.recyclerView);


    }


    /*After adding the bill move back the Bill summary fragment and update the view there.*/
    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.add_bill_btn:
                changeFragmentCallback.callFragment("ADD_BILL_FRAGMENT");
        }
    }

}
