package com.akonecttask.Utility;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by ${Rajeev} on 7/1/2017.
 */

public class Utility {


    public static void logDisplay(String title, String subTitle) {
        Log.i(title, subTitle);
    }

    public static final Drawable getDrawable(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {
            try {
                return ContextCompat.getDrawable(context, id);
            } catch (Exception e) {
                return null;
            }
        } else {
            return context.getResources().getDrawable(id);
        }
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}
