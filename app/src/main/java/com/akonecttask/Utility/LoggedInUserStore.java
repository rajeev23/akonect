package com.akonecttask.Utility;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ${Rajeev} on 7/1/2017.
 */

public class LoggedInUserStore {
    private Context context;
    SharedPreferences sharedPreferences;
    private static final String APP_PREF = "AKONECT";
    public static final int USERID = 1;


    public LoggedInUserStore(Context context) {
        this.context = context;
    }


    public static int getUserId(Context context) {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getInt("user_id", USERID);

    }

    public static void setUserId(Context context, int userId) {
        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putInt("user_id", USERID);
    }
}
