package com.akonecttask.Database;

/**
 * Created by ${Rajeev} on 7/1/2017.
 */

public class Queries {


    public static String createUserTable() {

        String query = "CREATE TABLE users (user_id INTEGER PRIMARY KEY NOT NULL,name TEXT NOT NULL,status INTEGER NOT NULL DEFAULT '1')";

        return query;
    }

    public static String createBillsTable() {

        String query = "CREATE TABLE bills (bill_id INTEGER PRIMARY KEY NOT NULL,user_id INTEGER NOT NULL,amount REAL NOT NULL, flag INTEGER NOT NULL DEFAULT '1', FOREIGN KEY(user_id) REFERENCES users(user_id))";
        return query;
    }

    public static String createSplitsTable() {
        String query = "CREATE TABLE splits (split_id INTEGER  PRIMARY KEY NOT NULL,bill_id INTEGER NOT NULL, user_id INTEGER NOT NULL, paid_by INTEGER NOT NULL, FOREIGN KEY(user_id) REFERENCES users(user_id), FOREIGN KEY(bill_id) REFERENCES bills(bill_id))";
        return query;
    }

    public static String createUsers(String name) {
        String query = "INSERT INTO users(name) values('" + name + "')";
        return query;
    }

}
