package com.akonecttask.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ${Rajeev} on 7/1/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String TAG = DatabaseHelper.class.getSimpleName();
    public static final String DATABASE_NAME = "AkonectTask.db";
    public static final String USER_TABLE = "users";
    public static final String BILL_TABLE = "bills";
    public static final String SPLIT_TABLE = "splits";

    SQLiteDatabase db;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        db = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Queries.createUserTable());
        db.execSQL(Queries.createUsers("rajeev"));
        db.execSQL(Queries.createUsers("david"));
        db.execSQL(Queries.createUsers("henry"));
        db.execSQL(Queries.createUsers("jerin"));
        db.execSQL(Queries.createBillsTable());
        db.execSQL(Queries.createSplitsTable());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

        db.execSQL("DROP TABLE IF EXISTS " + USER_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + BILL_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + SPLIT_TABLE);
        onCreate(db);
    }


    public Cursor getAllusers() {

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + USER_TABLE, null);
        return res;
    }

    public long saveBill(float amount, int user_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("amount", amount);
        contentValues.put("user_id", user_id);
        long res = db.insert(BILL_TABLE, null, contentValues);

        return res;
    }

    public void saveSplit(long bill_id, int user_id, int my_userId, boolean isPaidBy) {
        SQLiteDatabase db = this.getWritableDatabase();

        /*If the bill is paid by the user who created the bill, then the pay_by will be 1
         * else the paid by will be 0.
         * We insert 2 rows to this table so that we know how many people are involved in the
         * bill, and who paid for that particular bill
         * */
        String query1, query2;
        if (isPaidBy) {
            query1 = "insert into " + SPLIT_TABLE + "(bill_id,user_id,paid_by) values(" + bill_id + ", " + user_id + ",0)";
            query2 = "insert into " + SPLIT_TABLE + "(bill_id,user_id,paid_by) values(" + bill_id + ", " + my_userId + ",1)";

        } else {
            query1 = "insert into " + SPLIT_TABLE + "(bill_id,user_id,paid_by) values(" + bill_id + ", " + user_id + ",1)";
            query2 = "insert into " + SPLIT_TABLE + "(bill_id,user_id,paid_by) values(" + bill_id + ", " + my_userId + ",0)";
        }
        db.execSQL(query1);
        db.execSQL(query2);
    }

    /*1st Join - find out who all are invloved in the bill
    * 2nd join - find out bill details.(amount, bill_id etc)
    * 3rd join - Who created the bill.*/
    public Cursor fetchData(Context context) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "Select GROUP_CONCAT(shared_with.user_id) as shared_id, GROUP_CONCAT(shared_with.name) as shared_name, GROUP_CONCAT(" + SPLIT_TABLE + ".paid_by) AS paid_by, " +
                "created_by.user_id as created_id, created_by.name as created_name, " + BILL_TABLE + ".bill_id as bill_id, " + BILL_TABLE + ".amount from " + SPLIT_TABLE +
                " LEFT JOIN " + USER_TABLE + " AS shared_with ON shared_with.user_id = " + SPLIT_TABLE + ".user_id" +
                " LEFT JOIN " + BILL_TABLE + " ON " + BILL_TABLE + ".bill_id = " + SPLIT_TABLE + ".bill_id " +
                " LEFT JOIN " + USER_TABLE + " AS created_by ON created_by.user_id = " + BILL_TABLE + ".user_id  GROUP BY " + BILL_TABLE + ".bill_id";

        // Utility.logDisplay("QUERY =", query);
        Cursor cursor = db.rawQuery(query, null);
        return cursor;
    }
}
