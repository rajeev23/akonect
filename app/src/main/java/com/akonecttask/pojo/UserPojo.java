package com.akonecttask.pojo;

import java.io.Serializable;

/**
 * Created by ${Rajeev} on 7/1/2017.
 */

public class UserPojo implements Serializable {

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private int userId;
    private String username;
}
