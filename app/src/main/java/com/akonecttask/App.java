package com.akonecttask;

import android.app.Application;

import com.facebook.stetho.Stetho;

/**
 * Created by ${Rajeev} on 7/2/2017.
 */

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Stetho.initializeWithDefaults(this);


    }
}
