package com.akonecttask.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.akonecttask.R;
import com.akonecttask.Utility.LoggedInUserStore;
import com.akonecttask.pojo.AmountGetPojo;

import java.util.ArrayList;


/**
 * Bill Summary adapter for the recycler view.
 * Will get all the data from the local database
 * and populate it into our recycler view using this adapter.
 */

public class BillSummaryAdapter extends RecyclerView.Adapter<BillSummaryAdapter.ViewHolder> {
    private final Context mContext;
    ArrayList<AmountGetPojo> amountGetPojoArrayList;

    public BillSummaryAdapter(Context mContext, ArrayList<AmountGetPojo> amountGetPojoArrayList) {
        this.mContext = mContext;
        this.amountGetPojoArrayList = amountGetPojoArrayList;
    }


    @Override
    public BillSummaryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View myRecordView = inflater.inflate(R.layout.custom_adapter_layout, parent, false);
        return new ViewHolder(myRecordView);
    }

    @Override
    public void onBindViewHolder(BillSummaryAdapter.ViewHolder holder, int position) {

        TextView resultText = holder.et1;
        final Typeface custom_font = Typeface.createFromAsset(mContext.getAssets(), "fonts/Helvetica1.otf");
        resultText.setTypeface(custom_font);

        String myUserId = String.valueOf(LoggedInUserStore.getUserId(mContext));
        String status = amountGetPojoArrayList.get(position).getStatus();
        String pojoUserId = amountGetPojoArrayList.get(position).getUserId();
        String otherName = amountGetPojoArrayList.get(position).getOther();
        String userName = amountGetPojoArrayList.get(position).getUserName();
        float amount = amountGetPojoArrayList.get(position).getAmount();


        /*If this is the case then I get the money else i Owe the money*/
        if (status.equalsIgnoreCase("get")) {
            if (myUserId.equalsIgnoreCase(pojoUserId)) {
                resultText.setText(otherName + " owes you : " + amount);
            } else {
                resultText.setText("you owe " + userName + " : " + amount);
            }
        }
    }

    @Override
    public int getItemCount() {
        return amountGetPojoArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final TextView et1;

        public ViewHolder(View itemView) {
            super(itemView);
            et1 = (TextView) itemView.findViewById(R.id.textView1);
        }
    }
}
